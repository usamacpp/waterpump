package com.ossamamikhail.waterpump;

import java.util.ArrayList;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

public class SettingsBTActivity extends Activity {

	public class MyListViewItemAdapter extends BaseAdapter {

		private Context ctx;
		private ArrayList<BTData> listOfValues;
		private int selectedIndex = -1;

		public MyListViewItemAdapter(Context c, ArrayList<BTData> data) {
			ctx = c;
			listOfValues = data;
		}

		public View getView(final int position, View convertView,
				final ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// Creating a view of row.
			View rowView = inflater.inflate(R.layout.listcell_bt, null);

			final RadioButton rb = (RadioButton) rowView
					.findViewById(R.id.radioButton_Choose);
			rb.setText(this.listOfValues.get(position).Name);
			if (selectedIndex == position)
				rb.setChecked(true);
			else
				rb.setChecked(false);

			rb.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Log.d(TAG, "radio button pressed");

					selectedIndex = position;
					notifyDataSetChanged();
				}
			});

			TextView tv = (TextView) rowView.findViewById(R.id.textView_MAC);
			tv.setText(listOfValues.get(position).MAC);

			tv.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					rb.setChecked(true);

					selectedIndex = position;
					notifyDataSetChanged();
				}
			});

			return rowView;
		}

		@Override
		public int getCount() {
			return listOfValues.size();
		}

		@Override
		public Object getItem(int arg0) {
			return listOfValues.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}
	}

	BT bt;
	private String TAG = "WP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_bt);

		bt = new BT(this.getApplicationContext(), "20:13:05:06:45:87");

		this.updateList(bt.Enum());
	}

	private void updateList(ArrayList<BTData> lst) {
		if (lst != null)
			Log.d(TAG, "devs list size = " + lst.size());
		else
			Log.d(TAG, "list empty");

		ListView lv = (ListView) this.findViewById(R.id.listView_phones);

		MyListViewItemAdapter adapter = new MyListViewItemAdapter(this, lst);

		lv.setAdapter(adapter);
	}
}
