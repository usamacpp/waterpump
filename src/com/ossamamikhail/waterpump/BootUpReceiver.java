package com.ossamamikhail.waterpump;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootUpReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		setDetectEnabled(context);
	}

	private void setDetectEnabled(Context ctx) {

		Intent intent1 = new Intent(ctx,
				com.ossamamikhail.service.CallDetectService.class);
		Intent intent2 = new Intent(ctx,
				com.ossamamikhail.service.batterchargingMonitorService.class);
		Intent intent3 = new Intent(ctx,
				com.ossamamikhail.service.internetConnectionService.class);

		ctx.startService(intent1);
		ctx.startService(intent2);
		ctx.startService(intent3);
	}
}
