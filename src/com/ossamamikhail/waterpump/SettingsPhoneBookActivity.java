package com.ossamamikhail.waterpump;

import java.util.ArrayList;

import com.ossamamikhail.service.CallDetectService;
import com.ossamamikhail.service.batterchargingMonitorService;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

public class SettingsPhoneBookActivity extends Activity {

	public class MyListViewItemAdapter extends BaseAdapter {

		private Context ctx;
		private ArrayList<String> listOfValues;
		private int selectedIndex = -1;

		public MyListViewItemAdapter(Context c, ArrayList<String> data) {
			ctx = c;
			listOfValues = data;
		}

		public View getView(final int position, View convertView,
				final ViewGroup parent) {
			LayoutInflater inflater = (LayoutInflater) ctx
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// Creating a view of row.
			View rowView = inflater.inflate(R.layout.listcell_pb, null);

			final RadioButton rb = (RadioButton) rowView
					.findViewById(R.id.radioButton_Choose);
			rb.setText(this.listOfValues.get(position));
			if (selectedIndex == position)
				rb.setChecked(true);
			else
				rb.setChecked(false);

			rb.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Log.d(TAG, "radio button pressed");

					selectedIndex = position;

					notifyDataSetChanged();
				}
			});

			return rowView;
		}

		@Override
		public int getCount() {
			return listOfValues.size();
		}

		@Override
		public Object getItem(int arg0) {
			return listOfValues.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		public void DeleteSelectedIndex() {
			if (selectedIndex != -1) {
				listOfValues.set(selectedIndex, "");

				SharedPreferences pref = getSharedPreferences("MyPreferences",
						Context.MODE_PRIVATE);
				Editor edt = pref.edit();
				edt.putString("num" + (selectedIndex + 1), "");
				edt.commit();

				notifyDataSetChanged();
			}
		}

		public void SetSelectedIndexPhoneNumber(String PN) {
			if (selectedIndex != -1) {
				listOfValues.set(selectedIndex, PN);

				SharedPreferences pref = getSharedPreferences("MyPreferences",
						Context.MODE_PRIVATE);
				Editor edt = pref.edit();
				edt.putString("num" + (selectedIndex + 1), PN);
				edt.commit();

				notifyDataSetChanged();
			}
		}
	}

	BT bt;
	private String TAG = "WP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_phonebook);

		SharedPreferences pref = getApplicationContext().getSharedPreferences(
				"MyPreferences", Context.MODE_PRIVATE);

		String vnum1 = pref.getString("num1", "");
		String vnum2 = pref.getString("num2", "");
		String vnum3 = pref.getString("num3", "");
		String vnum4 = pref.getString("num4", "");
		String vnum5 = pref.getString("num5", "");

		ArrayList<String> data = new ArrayList<String>();

		data.add(vnum1);
		data.add(vnum2);
		data.add(vnum3);
		data.add(vnum4);
		data.add(vnum5);

		this.updateList(data);

		final Activity act = (Activity) this;
		Button btnAdd = (Button) this.findViewById(R.id.button_AddPhone);

		btnAdd.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				final EditText input = new EditText(act);
				input.setInputType(InputType.TYPE_CLASS_NUMBER);

				AlertDialog dlg = new AlertDialog.Builder(act)
						.setIcon(R.drawable.ic_launcher)
						.setTitle("Add Phone Number")
						.setMessage("Please enter Phone Number")
						.setView(input)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										if (!input.getText().toString()
												.isEmpty()) {
											ListView lv = (ListView) act
													.findViewById(R.id.listView_phones);
											((MyListViewItemAdapter) lv
													.getAdapter())
													.SetSelectedIndexPhoneNumber(input
															.getText()
															.toString());

											ResetSystemService();
										} else
											Toast.makeText(act, "Empty number",
													Toast.LENGTH_LONG).show();
									}
								}).setNegativeButton("Cancel", null).create();
				dlg.setOnShowListener(new DialogInterface.OnShowListener() {

					@Override
					public void onShow(DialogInterface arg0) {
						input.requestFocus();
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					}
				});

				dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {

					@Override
					public void onDismiss(DialogInterface arg0) {
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.toggleSoftInput(
								InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
					}
				});

				dlg.show();
			}
		});

		Button btnDel = (Button) this.findViewById(R.id.button_DelPhone);

		btnDel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Toast.makeText(act, "Deleted", Toast.LENGTH_LONG).show();
				ListView lv = (ListView) act.findViewById(R.id.listView_phones);
				((MyListViewItemAdapter) lv.getAdapter()).DeleteSelectedIndex();

				ResetSystemService();
			}
		});
	}

	private void ResetSystemService() {
		setDetectEnabled(false);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setDetectEnabled(true);
	}

	private void setDetectEnabled(boolean enable) {

		Intent intent1 = new Intent(this.getApplicationContext(),
				CallDetectService.class);

		if (enable) {
			// start detect service
			startService(intent1);
		} else {
			// stop detect service
			stopService(intent1);
		}
	}

	private void updateList(ArrayList<String> lst) {
		if (lst != null)
			Log.d(TAG, "devs list size = " + lst.size());
		else
			Log.d(TAG, "list empty");

		ListView lv = (ListView) this.findViewById(R.id.listView_phones);

		MyListViewItemAdapter adapter = new MyListViewItemAdapter(this, lst);

		lv.setAdapter(adapter);

		// MyListViewItemAdapter lvadptr =
		// (MyListViewItemAdapter)lv.getAdapter();
	}
}
