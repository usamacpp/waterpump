package com.ossamamikhail.waterpump;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SmsReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		// ---get the SMS message passed in---
		Bundle bundle = intent.getExtras();
		SmsMessage[] msg = null;
		String str = "";
		String TAG = "WP";

		Log.d(TAG, "SMS RECV OK");

		if (bundle != null) {
			Object[] pdus = (Object[]) bundle.get("pdus");
			msg = new SmsMessage[pdus.length];

			for (int i = 0; i < msg.length; i++) {
				msg[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
				str += "SMS from " + msg[i].getOriginatingAddress();
				str += " :" + msg[i].getMessageBody().toString() + "\n";

				SharedPreferences pref = context.getSharedPreferences(
						"MyPreferences", Context.MODE_PRIVATE);

				String callernum = msg[i].getOriginatingAddress();
				String vnum1 = pref.getString("num1", "");
				String vnum2 = pref.getString("num2", "");
				String vnum3 = pref.getString("num3", "");
				String vnum4 = pref.getString("num4", "");
				String vnum5 = pref.getString("num5", "");

				if ((callernum.endsWith(vnum1) && !vnum1.isEmpty())
						|| (callernum.endsWith(vnum2) && !vnum2.isEmpty())
						|| (callernum.endsWith(vnum3) && !vnum3.isEmpty())
						|| (callernum.endsWith(vnum4) && !vnum4.isEmpty())
						|| (callernum.endsWith(vnum5) && !vnum5.isEmpty())) {
					try {
						Log.d(TAG, "Got correct sms to check motor status");

						Intent intnt = new Intent(
								"com.ossamamikhail.service.SendSMSActivity");
						Bundle b = new Bundle();
						b.putString("phoneno", msg[i].getOriginatingAddress());
						b.putString("status", "OK");
						b.putString("order", msg[i].getMessageBody()
								.toLowerCase());
						intnt.putExtras(b);
						intnt.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						context.getApplicationContext().startActivity(intnt);
					} catch (Exception e) {
						Log.d(TAG, "recv sms error - " + e.getMessage());
					}
				} else
					Log.d(TAG, "SMS sender not allowed");
			}
		}
	}
}