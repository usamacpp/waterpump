package com.ossamamikhail.waterpump;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

public class BT {

	private Context ctx;

	// SPP UUID service
	public static final UUID MY_UUID = UUID
			.fromString("00001101-0000-1000-8000-00805F9B34FB");

	// MAC-address of Bluetooth module (you must edit this line)
	public static String address = "20:14:03:19:20:48";

	BluetoothAdapter ad = BluetoothAdapter.getDefaultAdapter();
	public Set<BluetoothDevice> bdevs;

	BluetoothDevice dev;
	BluetoothSocket soc;

	private String TAG = "WP";

	public BT(Context actx, String a) {
		ctx = actx;
		// address = a;
	}

	public ArrayList<BTData> Enum() {
		try {
			ad = BluetoothAdapter.getDefaultAdapter();
			ArrayList<BTData> devs = new ArrayList<BTData>();

			if (ad != null) {
				if (ad.isEnabled()) {
					bdevs = ad.getBondedDevices();

					for (BluetoothDevice dev : bdevs) {
						BTData data = new BTData(dev.getAddress(),
								dev.getName());
						devs.add(data);
					}
				} else
					Toast.makeText(ctx, "BT disabled", Toast.LENGTH_LONG)
							.show();
			} else
				Toast.makeText(ctx, "No BT", Toast.LENGTH_LONG).show();

			return devs;
		} catch (Exception e) {
			Log.d(TAG, "BT Enum - Error - " + e.getMessage());
		}

		return null;
	}

	public boolean Start() {
		try {
			if (ad == null) {
				Toast.makeText(ctx, "BT disabled", Toast.LENGTH_LONG).show();
				return false;
			}

			if (!ad.isEnabled()) {
				Toast.makeText(ctx, "No BT", Toast.LENGTH_LONG).show();
				return false;
			}

			// get our device
			dev = ad.getRemoteDevice(address);
			soc = null;

			try {
				soc = createBluetoothSocket(dev);

				soc.connect();

				if (soc.getInputStream().available() > 0)
					soc.getInputStream().reset();
			} catch (IOException e) {
				Log.d(TAG, "BT Start - comms error - " + e.getMessage());
				return false;
			}

			return true;
		} catch (Exception e) {
			Log.d(TAG, "BT Start - Error - " + e.getMessage());
		}

		return false;
	}

	public byte SendCommand(byte data) {

		if (soc == null) {
			Toast.makeText(ctx, "Socket N/A", Toast.LENGTH_LONG).show();
			return 0;
		}

		try {
			byte[] ob = new byte[1];
			byte[] ib = new byte[2];

			ob[0] = data;

			Log.d(TAG, String.format("Send data via BT = %X", data));
			soc.getOutputStream().write(ob);
			Thread.sleep(200);

			long t1 = System.currentTimeMillis();

			while (true) {
				if (soc.getInputStream().available() > 0) {
					soc.getInputStream().read(ib);
					Log.d(TAG, String.format("Recv data via BT = %X", ib[0]));

					soc.close();
					return ib[0];
				}

				long t2 = System.currentTimeMillis();

				if ((t2 - t1) >= 5000) {
					Log.d(TAG, "BT NO recv");
					break;
				}
			}
			return 0;
		} catch (IOException e) {
			Log.d(TAG, "BT SendCommand - comms error - " + e.getMessage());
		} catch (InterruptedException e) {
			Log.d(TAG, "BT SendCommand - comms error - " + e.getMessage());
		}

		return 0;
	}

	public void Stop() {
		try {
			soc.close();
			soc = null;
		} catch (IOException e) {
			Log.d(TAG, "Socket stop failed");
			e.printStackTrace();
		}
	}

	private BluetoothSocket createBluetoothSocket(BluetoothDevice device)
			throws IOException {
		if (Build.VERSION.SDK_INT >= 10) {
			try {
				final Method m = device.getClass().getMethod(
						"createInsecureRfcommSocketToServiceRecord",
						new Class[] { UUID.class });
				return (BluetoothSocket) m.invoke(device, MY_UUID);
			} catch (Exception e) {
				Log.e(TAG, "Could not create Insecure RFComm Connection", e);
			}
		}

		return device.createRfcommSocketToServiceRecord(MY_UUID);
	}
}
