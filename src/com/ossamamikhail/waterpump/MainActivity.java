package com.ossamamikhail.waterpump;

import java.io.IOException;

import com.ossamamikhail.service.CallDetectService;
import com.ossamamikhail.service.MySocket;
import com.ossamamikhail.service.batterchargingMonitorService;
import com.ossamamikhail.service.internetConnectionService;

import android.os.Bundle;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	BT bt;
	private String TAG = "WP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// SharedPreferences pref = getSharedPreferences("MyPreferences",
		// Context.MODE_PRIVATE);
		// Editor edt = pref.edit();
		// edt.putString("num1", "01111658222");
		// edt.commit();

		// SmsSender sms = new SmsSender(this.getBaseContext());

		// sms.sendSMS("01111658222", "OK");

		TextView txtv = (TextView) this.findViewById(R.id.textView_MAC);
		Button btn1 = (Button) this.findViewById(R.id.button_ON);
		Button btn2 = (Button) this.findViewById(R.id.button_OFF);
		Button btn3 = (Button) this.findViewById(R.id.button_RunService);

		setDetectEnabled(true);

		bt = new BT(this, "20:14:03:19:20:48");

		btn1.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

				Thread th1 = new Thread(new Runnable() {
					public void run() {
						try {
							MySocket soc;
							soc = new MySocket("192.30.160.116", 50000);
							String recv = soc.Send("OK1");
							Log.d(TAG, "Data recv = " + recv);
						} catch (IOException e1) {
							Log.d(TAG, e1.getMessage());
						}
					}
				});

				th1.start();

				try {
					if (bt != null) {
						bt.Start();
						bt.SendCommand((byte) 0x55);
						bt.Stop();
					} else
						Toast.makeText(getApplicationContext(), "NO BT",
								Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "BT Error",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		btn2.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {
					if (bt != null) {
						bt.Start();
						bt.SendCommand((byte) 0x60);
						bt.Stop();
					} else
						Toast.makeText(getApplicationContext(), "NO BT",
								Toast.LENGTH_LONG).show();
				} catch (Exception e) {
					Toast.makeText(getApplicationContext(), "BT Error",
							Toast.LENGTH_LONG).show();
				}
			}
		});

		btn3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (!isMyServiceRunning())
					setDetectEnabled(true);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings_bt) {
			final EditText input = new EditText(this);
			input.setInputType(InputType.TYPE_CLASS_NUMBER
					| InputType.TYPE_NUMBER_VARIATION_PASSWORD);

			AlertDialog dlg = new AlertDialog.Builder(this)
					.setIcon(R.drawable.ic_launcher)
					.setTitle("Passcode")
					.setMessage("Please enter passcode")
					.setView(input)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									if (input.getText().toString()
											.equals("1212")) {
										startActivity(new Intent(
												"com.ossamamikhail.waterpump.SettingsBTActivity"));
									} else
										Toast.makeText(getBaseContext(),
												"Wrong Passcode",
												Toast.LENGTH_LONG).show();
								}
							}).setNegativeButton("Cancel", null).create();
			dlg.setOnShowListener(new DialogInterface.OnShowListener() {

				@Override
				public void onShow(DialogInterface arg0) {
					input.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				}
			});

			dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface arg0) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
							0);
				}
			});

			dlg.show();
			return true;
		}

		if (id == R.id.action_settings_pb) {
			final EditText input = new EditText(this);
			input.setInputType(InputType.TYPE_CLASS_NUMBER
					| InputType.TYPE_NUMBER_VARIATION_PASSWORD);

			AlertDialog dlg = new AlertDialog.Builder(this)
					.setIcon(R.drawable.ic_launcher)
					.setTitle("Passcode")
					.setMessage("Please enter passcode")
					.setView(input)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface arg0,
										int arg1) {
									if (input.getText().toString()
											.equals("1212")) {
										startActivity(new Intent(
												"com.ossamamikhail.waterpump.SettingsPhoneBookActivity"));
									} else
										Toast.makeText(getBaseContext(),
												"Wrong Passcode",
												Toast.LENGTH_LONG).show();
								}
							}).setNegativeButton("Cancel", null).create();
			dlg.setOnShowListener(new DialogInterface.OnShowListener() {

				@Override
				public void onShow(DialogInterface arg0) {
					input.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				}
			});

			dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface arg0) {
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY,
							0);
				}
			});

			dlg.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void setDetectEnabled(boolean enable) {

		Intent intent1 = new Intent(this.getApplicationContext(),
				CallDetectService.class);
		Intent intent2 = new Intent(this.getApplicationContext(),
				batterchargingMonitorService.class);
		Intent intent3 = new Intent(this.getApplicationContext(),
				internetConnectionService.class);

		if (enable) {
			// start detect service
			startService(intent1);
			startService(intent2);
			startService(intent3);
		} else {
			// stop detect service
			stopService(intent1);
			stopService(intent2);
			stopService(intent3);
		}
	}

	private boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		for (RunningServiceInfo serviceInfo : manager
				.getRunningServices(Integer.MAX_VALUE)) {

			if ("com.ossamamikhail.service.CallDetectService"
					.equals(serviceInfo.service.getClassName())) {
				return true;
			}

			if ("com.ossamamikhail.service.CallDetectService"
					.equals(serviceInfo.process)) {
				return true;
			}
		}

		return false;
	}
}
