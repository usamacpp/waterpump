package com.ossamamikhail.waterpump;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

public class SmsSender {
	Context context;

	String SENT = "SMS_SENT";
	String DELIVERED = "SMS_DELIVERED";

	String tag = "WP";

	SmsManager smsManager;
	PendingIntent sendPI;
	PendingIntent deliveredPI;

	public SmsSender(Context ctx) {
		context = ctx;

		sendPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT), 0);
		deliveredPI = PendingIntent.getBroadcast(context, 0, new Intent(
				DELIVERED), 0);

		// ---when the SMS has been sent---

		context.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(context, "SMS Sent", Toast.LENGTH_LONG)
							.show();
					Log.d(tag, "SMS Sent");
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(context, "Generic Failure",
							Toast.LENGTH_LONG).show();
					Log.d(tag, "SMS Generic Failure");
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(context, "No Service", Toast.LENGTH_LONG)
							.show();
					Log.d(tag, "SMS No Service");
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(context, "Null PDU", Toast.LENGTH_LONG)
							.show();
					Log.d(tag, "SMS Null PDU");
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(context, "Radio Off", Toast.LENGTH_LONG)
							.show();
					Log.d(tag, "SMS Rdio Off");
					break;
				default:
					break;
				}
			}
		}, new IntentFilter(SENT));

		context.registerReceiver(new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(context, "SMS Delivered", Toast.LENGTH_LONG)
							.show();
					Log.d(tag, "SMS Delivered");
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(context, "SMS Not Delivered",
							Toast.LENGTH_LONG).show();
					Log.d(tag, "SMS Not Delivered");
					break;
				default:
					break;
				}

			}

		}, new IntentFilter(DELIVERED));

		smsManager = SmsManager.getDefault();
	}

	// ---sends an SMS message to another device---
	public void sendSMS(String phoneNumber, String message) {
		try {
			smsManager.sendTextMessage(phoneNumber, null, message, sendPI,
					deliveredPI);
			// smsManager.sendTextMessage(phoneNumber, null, message, null,
			// null);
		} catch (Exception e) {
			Log.e(tag, "SendSms() error : " + e.getMessage());
		}
	}
}
