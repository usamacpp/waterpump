package com.ossamamikhail.service;

import java.io.IOException;

import com.ossamamikhail.waterpump.BT;
import com.ossamamikhail.waterpump.SmsSender;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class internetConnectionService extends Service {
	private String TAG = "WP";

	boolean bRun = true;
	int nServiceStarted = 0;
	Context ctx;
	Thread th;

	public internetConnectionService() {
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "Water Pump INTERNET Service Created");
		nServiceStarted = 0;
		ctx = this.getApplicationContext();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		try {
			nServiceStarted++;

			if (nServiceStarted > 1) {
				Log.d(TAG, "INTERNET - extra service killed");
				this.finalize();
				return -1;
			}

			int res = super.onStartCommand(intent, flags, startId);

			th = new Thread() {
				@Override
				public void run() {

					while (true) {
						try {
							MySocket csoc = new MySocket("192.30.160.116",
									50000);

							String recv = csoc.Send("IMEI = ");
							Log.d(TAG, "recv data = " + recv);
							Sleep();
						} catch (Exception e) {
							Log.d(TAG,
									"INTERNET Service error - "
											+ e.getMessage());
						}

						Sleep();
					}
				}
			};

			th.start();

			Toast.makeText(this.getBaseContext(),
					"Water Pump INTERNET service STARTED", Toast.LENGTH_LONG)
					.show();
			Log.d(TAG, "Water Pump INTERNET service STARTED");
			return res;

		} catch (Throwable e) {
			Log.d(TAG,
					"INTERNET Service - onStartCommand Error - "
							+ e.getMessage());
		}

		return -1;
	}

	private void Sleep() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		th.stop();

		Log.d(TAG, "Water Pump INTERNET Service Destroyed");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// not supporting binding
		return null;
	}
}
