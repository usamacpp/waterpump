package com.ossamamikhail.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import android.util.Log;

public class MySocketSender {
	public InetAddress serverAddr;
	public Socket s;
	private String sip;
	private int sport;
	public boolean bFailed = false;
	String TAG = "WP";

	class ClientThread implements Runnable {

		@Override
		public void run() {

			try {
				serverAddr = InetAddress.getByName(sip);
				s = new Socket(serverAddr, sport);

				bFailed = false;

			} catch (UnknownHostException e1) {
				e1.printStackTrace();
				Log.d(TAG, "tcp connect error");
				bFailed = true;
			} catch (IOException e1) {
				e1.printStackTrace();
				Log.d(TAG, "tcp connect error");
				bFailed = true;
			}
		}
	}

	public MySocketSender(String ip, int port) throws IOException {
		sip = ip;
		sport = port;

		new Thread(new ClientThread()).start();
	}

	public String Send(String cmd) throws IOException {
		if (s != null) {
			if (s.isConnected()) {
				PrintWriter out = new PrintWriter(new BufferedWriter(
						new OutputStreamWriter(s.getOutputStream())), true);
				out.print(cmd);
				out.flush();

				BufferedReader input = new BufferedReader(
						new InputStreamReader(s.getInputStream()));

				return input.readLine();
			}
		}

		return "";
	}

	public String Recv() throws IOException {
		if (s != null) {
			if (s.isConnected()) {
				BufferedReader input = new BufferedReader(
						new InputStreamReader(s.getInputStream()));

				return input.readLine();
			}
		}

		return "";
	}

	public void Stop() {
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
			Log.d(TAG, "tcp stop error");
		}
	}
}
