package com.ossamamikhail.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import android.util.Log;

public class MySocket {
	public InetAddress serverAddr;
	public Socket s;
	String TAG = "WP";

	public MySocket(String ip, int port) throws IOException {
		serverAddr = InetAddress.getByName(ip);
		s = new Socket(serverAddr, port);

		// SocketAddress remoteAddr = new
		// InetSocketAddress(InetAddress.getByName(ip), port);
		// s.connect(remoteAddr, 2000);
	}

	public String Send(String cmd) throws IOException {
		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(
					new OutputStreamWriter(s.getOutputStream())), true);
			out.println(cmd);
			out.flush();

			Sleep();

			BufferedReader input = new BufferedReader(new InputStreamReader(
					s.getInputStream()));

			String recv = input.readLine();

			s.close();

			return recv;
		} catch (Exception e) {
			Log.d(TAG,
					"INTERNET service Send Error =============> "
							+ e.getMessage());
		}
		return "";
	}

	private void Sleep() {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void Stop() {
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
