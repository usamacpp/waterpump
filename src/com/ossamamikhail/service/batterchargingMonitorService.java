package com.ossamamikhail.service;

import com.ossamamikhail.waterpump.BT;
import com.ossamamikhail.waterpump.SmsSender;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class batterchargingMonitorService extends Service {
	private String TAG = "WP";

	boolean bRun = true;
	int nServiceStarted = 0;
	Context ctx;
	Thread th;

	boolean shouldCharge = true;
	byte auxRelaycurrValue = (byte) 0x00;

	public batterchargingMonitorService() {
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "Water Pump Battery Monitor Service Created");
		nServiceStarted = 0;
		ctx = this.getApplicationContext();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		try {
			nServiceStarted++;

			if (nServiceStarted > 1) {
				Log.d(TAG, "batter charge monitior - extra service killed");
				this.finalize();
				return -1;
			}

			int res = super.onStartCommand(intent, flags, startId);

			th = new Thread() {
				@Override
				public void run() {

					IntentFilter ifilter = new IntentFilter(
							Intent.ACTION_BATTERY_CHANGED);
					Intent batteryStatus = ctx.registerReceiver(null, ifilter);

					while (true) {
						try {
							int level = batteryStatus.getIntExtra(
									BatteryManager.EXTRA_LEVEL, -1);
							int scale = batteryStatus.getIntExtra(
									BatteryManager.EXTRA_SCALE, -1);

							float batteryPct = level / (float) scale;

							int status = batteryStatus.getIntExtra(
									BatteryManager.EXTRA_STATUS, -1);
							boolean isCharging = false;

							if (status == BatteryManager.BATTERY_STATUS_CHARGING)
								isCharging = true;
							if (status == BatteryManager.BATTERY_STATUS_FULL)
								isCharging = false;

							Log.d(TAG, "battery level = " + batteryPct
									+ ", is charging? "
									+ (isCharging ? "YES" : "NO"));

							try {
								if (shouldCharge) {
									if (batteryPct == 1.0f) {
										if (auxRelaycurrValue != 0x20) {
											// bt.SendCommand((byte)0x20);
											DoBT((byte) 0x20);
											auxRelaycurrValue = (byte) 0x20;

											shouldCharge = false;
											Log.d(TAG,
													"Battery charging aux relay OFF");
										}
									}

									if (batteryPct < 1.0f) {
										if (auxRelaycurrValue != 0x10) {
											// bt.SendCommand((byte)0x10);
											DoBT((byte) 0x10);
											auxRelaycurrValue = (byte) 0x10;

											shouldCharge = true;
											Log.d(TAG,
													"Battery charging aux relay ON");
										}
									}
								} else {
									if (batteryPct <= 0.1f) {
										if (auxRelaycurrValue != 0x10) {
											// bt.SendCommand((byte)0x10);
											DoBT((byte) 0x10);
											auxRelaycurrValue = (byte) 0x10;

											shouldCharge = true;
											Log.d(TAG,
													"Battery charging aux relay ON");
										}
									}
								}
							} catch (Exception e) {
								Log.d(TAG, "BT comms error = " + e.getMessage());
							}

							sleep(5000);
						} catch (Exception e) {
							Log.d(TAG, "battery charge monitior - error");
						}
					}
				}
			};

			th.start();

			Toast.makeText(this.getBaseContext(),
					"Water Pump Battery Monitor service STARTED",
					Toast.LENGTH_LONG).show();
			Log.d(TAG, "Water Pump Battery Monitor service STARTED");
			return res;

		} catch (Throwable e) {
			Log.d(TAG, "batterchargingMonitorService - onStartCommand Error - "
					+ e.getMessage());
		}

		return -1;
	}

	private void DoBT(byte cmd) {
		try {
			BT bt = new BT(ctx.getApplicationContext(), "20:14:03:19:20:48");

			if (!bt.Start()) {
				Log.d(TAG, "BT Start error");
				return;
			}

			bt.SendCommand((byte) cmd);

			bt.Stop();
		} catch (Exception e) {
			Log.d(TAG,
					"batterchargingMonitorService - DoBT Error - "
							+ e.getMessage());
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		th.stop();

		Log.d(TAG, "Water Pump Battery Monitor Service Destroyed");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// not supporting binding
		return null;
	}
}
