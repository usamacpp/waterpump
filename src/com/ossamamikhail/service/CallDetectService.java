package com.ossamamikhail.service;

import java.io.IOException;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class CallDetectService extends Service {
	private String TAG = "WP";
	private CallHelper callHelper;

	boolean bRun = true;
	int nServiceStarted = 0;
	Context ctx;

	public CallDetectService() {
	}

	@Override
	public void onCreate() {
		Log.d(TAG, "Water Pump Call Receiver Service Created");
		nServiceStarted = 0;
		ctx = this.getApplicationContext();

		callHelper = new CallHelper(ctx);
		callHelper.start();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		try {
			nServiceStarted++;

			if (nServiceStarted > 1) {
				Log.d(TAG, "Call Receive - extra service killed");
				this.finalize();
				return -1;
			}

			int res = super.onStartCommand(intent, flags, startId);
			// callHelper = new CallHelper(this);
			// callHelper.start();

			Toast.makeText(this.getBaseContext(),
					"Water Pump Call recv service STARTED", Toast.LENGTH_LONG)
					.show();

			Log.d(TAG, "Water Pump Call recv service STARTED");
			Log.d(TAG, "Water Pump Call recv service running count = "
					+ nServiceStarted);

			return res;

		} catch (Throwable e) {
			Log.d(TAG,
					"CallDetectService - onStartCommand Error - "
							+ e.getMessage());
		}

		return -1;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		callHelper.stop();

		nServiceStarted = 0;

		Log.d(TAG, "Water Pump Battery Monitor Service Destroyed");
	}

	@Override
	public IBinder onBind(Intent intent) {
		// not supporting binding
		return null;
	}
}
