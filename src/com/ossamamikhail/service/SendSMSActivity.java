package com.ossamamikhail.service;

import java.util.ArrayList;

import com.ossamamikhail.waterpump.BT;
import com.ossamamikhail.waterpump.R;
import com.ossamamikhail.waterpump.SmsSender;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class SendSMSActivity extends Activity {

	private String TAG = "WP";

	BT bt;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_sendsms);

		try {

			Log.d(TAG, "control & send sms act");

			Bundle b = this.getIntent().getExtras();

			String phoneno = (String) b.get("phoneno");
			String status = (String) b.get("status");
			String order = (String) b.get("order");

			bt = new BT(this.getApplicationContext(), "20:14:03:19:20:48");

			if (bt == null) {
				Log.d(TAG, "BT error");
				this.finish();
				return;
			}

			if (!bt.Start()) {
				Log.d(TAG, "BT Start error");
				SmsSender sms = new SmsSender(this);
				sms.sendSMS(phoneno, "MAINT BT");
				sms = null;
				this.finish();
				return;
			}

			Thread.sleep(5000);

			byte rt = 0;

			if (order.equals("flip"))
				rt = bt.SendCommand((byte) 0xFF);

			if (order.equals("check"))
				rt = bt.SendCommand((byte) 0xAA);

			if (order.equals("on"))
				rt = bt.SendCommand((byte) 0x55);

			if (order.equals("off"))
				rt = bt.SendCommand((byte) 0x60);

			bt.Stop();

			String sstatus = "N/A";

			/*
			 * if(rt == (byte)0x01) sstatus = "OFF"; if(rt == (byte)0xFF)
			 * sstatus = "ON";
			 */

			if (rt == (byte) 0x00) {
				sstatus = "MAINT";
			} else {
				if (order.equals("on")) {
					if (rt == (byte) 0xFF)
						sstatus = "شغال";
					else
						sstatus = "صيانة";
				}

				if (order.equals("off")) {
					if (rt == (byte) 0x01)
						sstatus = "واقف";
					else
						sstatus = "صيانة";
				}

				if (order.equals("flip")) {
					if (rt == (byte) 0xFF)
						sstatus = "شغال";

					if (rt == (byte) 0x01)
						sstatus = "واقف";
				}

				if (order.equals("check")) {
					if (rt == (byte) 0xFF)
						sstatus = "شغال";

					if (rt == (byte) 0x01)
						sstatus = "واقف";
				}
			}

			SmsSender sms = new SmsSender(this);
			sms.sendSMS(phoneno, sstatus);
			sms = null;

			Log.d(TAG, "STATUS = " + sstatus);
			Toast.makeText(this.getApplicationContext(), "STATUS = " + sstatus,
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Log.d(TAG, "SendSMSActivity - error - " + e.getMessage());
		}

		this.finish();
	}
}
