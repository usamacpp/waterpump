package com.ossamamikhail.service;

import java.io.IOException;
import java.lang.reflect.Method;

import com.android.internal.telephony.ITelephony;
import com.ossamamikhail.waterpump.BT;
import com.ossamamikhail.waterpump.SmsSender;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Helper class to detect incoming and outgoing calls.
 * 
 * @author Moskvichev Andrey V.
 * 
 */
public class CallHelper {
	private String TAG = "WP";

	/**
	 * Listener to detect incoming calls.
	 */
	private class CallStateListener extends PhoneStateListener {
		@Override
		public void onCallStateChanged(int state, final String incomingNumber) {

			try {
				switch (state) {
				case TelephonyManager.CALL_STATE_RINGING:
					// called when someone is ringing to this phone

					Toast.makeText(
							ctx,
							"Incoming captured by WaterPump app: "
									+ incomingNumber, Toast.LENGTH_LONG).show();

					SharedPreferences pref = ctx.getSharedPreferences(
							"MyPreferences", Context.MODE_PRIVATE);

					String vnum1 = pref.getString("num1", "");
					String vnum2 = pref.getString("num2", "");
					String vnum3 = pref.getString("num3", "");
					String vnum4 = pref.getString("num4", "");
					String vnum5 = pref.getString("num5", "");

					if (incomingNumber.equals(vnum1)) {
						Log.d(TAG,
								"Caller identified ========>>>>>>>>>>>>>>>>>");

						DoControlWork(incomingNumber);
					} else if (incomingNumber.equals(vnum2)) {
						Log.d(TAG,
								"Caller identified ========>>>>>>>>>>>>>>>>>");

						DoControlWork(incomingNumber);
					} else if (incomingNumber.equals(vnum3)) {
						Log.d(TAG,
								"Caller identified ========>>>>>>>>>>>>>>>>>");

						DoControlWork(incomingNumber);
					} else if (incomingNumber.equals(vnum4)) {
						Log.d(TAG,
								"Caller identified ========>>>>>>>>>>>>>>>>>");

						DoControlWork(incomingNumber);
					} else if (incomingNumber.equals(vnum5)) {
						Log.d(TAG,
								"Caller identified ========>>>>>>>>>>>>>>>>>");

						DoControlWork(incomingNumber);
					} else {
						Log.d(TAG,
								"Caller NOT identified ========>>>>>>>>>>>>>>>>>");

						RejectCall();
					}

					pref = null;

					Thread th1 = new Thread(new Runnable() {
						public void run() {
							try {
								MySocket soc;
								soc = new MySocket("192.30.160.116", 50000);
								soc.Send("Incoming call from " + incomingNumber);
							} catch (IOException e1) {
								Log.d(TAG, e1.getMessage());
							}
						}
					});

					th1.start();

					break;
				}
			} catch (Exception e) {
				Log.d(TAG, "onCallStateChanged Error - " + e.getMessage());
			}
		}
	}

	/**
	 * Broadcast receiver to detect the outgoing calls.
	 */
	public class OutgoingReceiver extends BroadcastReceiver {
		public OutgoingReceiver() {
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

			Toast.makeText(ctx, "Outgoing captured by usama app: " + number,
					Toast.LENGTH_LONG).show();
		}

	}

	private Context ctx;
	private TelephonyManager tm;
	private CallStateListener callStateListener;

	private OutgoingReceiver outgoingReceiver;

	public CallHelper(Context ctx) {
		this.ctx = ctx;

		callStateListener = new CallStateListener();
		outgoingReceiver = new OutgoingReceiver();
	}

	/**
	 * Start calls detection.
	 */
	public void start() {
		tm = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);
		tm.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

		IntentFilter intentFilter = new IntentFilter(
				Intent.ACTION_NEW_OUTGOING_CALL);
		ctx.registerReceiver(outgoingReceiver, intentFilter);
	}

	/**
	 * Stop calls detection.
	 */
	public void stop() {
		tm.listen(callStateListener, PhoneStateListener.LISTEN_NONE);
		ctx.unregisterReceiver(outgoingReceiver);
	}

	public void RejectCall() {
		try {
			Thread.sleep(3000);
			Class c = Class.forName(tm.getClass().getName());
			Method m = c.getDeclaredMethod("getITelephony");
			m.setAccessible(true);
			ITelephony telephonyService = (ITelephony) m.invoke(tm);
			// telephonyService.silenceRinger();
			telephonyService.endCall();
		} catch (Exception e) {
			Log.d(TAG, "Reject Call Error");
			Log.d(TAG, e.getMessage());
		}
	}

	public void DoControlWork(String phoneNo) {
		try {
			RejectCall();
			Log.d(TAG, "Call rejected");

			Intent i = new Intent("com.ossamamikhail.service.SendSMSActivity");
			Bundle b = new Bundle();
			b.putString("phoneno", phoneNo);
			b.putString("status", "OK");
			b.putString("order", "flip");
			i.putExtras(b);
			i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			ctx.getApplicationContext().startActivity(i);
		} catch (Exception e) {
			Log.d(TAG, "Control Work Error");
			Log.d(TAG, e.getMessage());
		}
	}
}
